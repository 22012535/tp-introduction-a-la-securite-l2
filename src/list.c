#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};

typedef struct cell_t* list_t;

list_t list_empty(){
  return NULL;
}

int list_is_empty(list_t l){
  return l == NULL;
}

list_t list_push(list_t l, void* x){

  list_t tmp = malloc(sizeof(struct cell_t)); //pourquoi pas list_t?
  tmp->val = x;
  tmp->next = l;

  if(list_is_empty(l)) {
    tmp->id = 1;
    return tmp;
  }
  tmp->id = l->id+1;
  return tmp;
}

list_t list_tail(list_t l){
  return l->next;
}

void* list_top(list_t l){
  return l->val;
}

void* list_pop(list_t* l){ 

  if (list_is_empty(*l)){
    return NULL;
  }

  list_t premierElement = *l;
  void * val = list_top(premierElement);
  *l = list_tail(premierElement);
  free(premierElement);

  return val;
}



void list_destroy(list_t l, void (*freeVoid)(void*)){

  list_t next;

  while(l->next != NULL) {
    next = list_tail(l);
    freeVoid(l->val);
    free(l);
    l = next;
  }
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)){

  if (list_empty(l)){
    return NULL;
  }

  list_t tmp = l;

  while(tmp != NULL) {

    if(eq(tmp->val,x)) {
      return tmp->val;
    }
    tmp = tmp->next;
  }
  return NULL;
}

unsigned long int list_len(list_t l){
  return l->id;
  
}

